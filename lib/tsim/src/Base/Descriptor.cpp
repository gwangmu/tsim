#include <TSim/Base/Descriptor.h>

#include <TSim/Base/Component.h>
#include <TSim/Pathway/Pathway.h>
#include <TSim/Pathway/Endpoint.h>
#include <TSim/Pathway/Wire.h>
#include <TSim/Pathway/FanoutWire.h>
#include <TSim/Pathway/RRFaninWire.h>

using namespace std;

namespace TSim
{
    enum AssignOperandType { ASOPER_LVALUE, ASOPER_RVALUE };

    /* logger */
    static void PrintPortAssignValidity (Descriptor *desc, AssignOperandType asoptype)
    {
        if (PortListDescriptor *pldesc = dynamic_cast<PortListDescriptor *>(desc))
        {
            for (auto i = 0; i < pldesc->pdescs.size(); i++)
                PrintPortAssignValidity (&pldesc->pdescs[i], asoptype);
        }
        else if (PortDescriptor *pdesc = dynamic_cast<PortDescriptor *>(desc))
        {
            Component::PortType ptype = pdesc->comp->GetPortType(pdesc->pname);

            if (asoptype == ASOPER_RVALUE)
            {
                if (ptype != Component::PORT_OUTPUT)
                {
                    DESIGN_ERROR ("r-value of port-assignment must be "
                            "RHS endpoint or OUTPUT port",
                            pdesc->owner->GetFullName().c_str());
                    DESIGN_INFO ("port %s is not OUTPUT port",
                            pdesc->pname.c_str(), pdesc->owner->GetFullName().c_str());
                }
            }
            else if (asoptype == ASOPER_LVALUE)
            {
                if (ptype != Component::PORT_INPUT && ptype != Component::PORT_CONTROL)
                {
                    DESIGN_ERROR ("l-value of port-assignment must be "
                            "LHS endpoint or INPUT/CONTROL port",
                            pdesc->owner->GetFullName().c_str());
                    DESIGN_INFO ("port %s is not INPUT/CONTROL port",
                            pdesc->pname.c_str(), pdesc->owner->GetFullName().c_str());
                }
            }
        }
        else if (EndpointDescriptor *edesc = dynamic_cast<EndpointDescriptor *>(desc))
        {
            Endpoint::Type etype = edesc->endpt->GetEndpointType();

            if (asoptype == ASOPER_RVALUE)
            {
                if (etype != Endpoint::RHS)
                {
                    DESIGN_ERROR ("r-value of port-assignment must be "
                            "RHS endpoint or OUTPUT port",
                            edesc->owner->GetFullName().c_str());
                    DESIGN_INFO ("endpoint %s is not RHS endpoint",
                            edesc->endpt->GetName().c_str(), 
                            edesc->owner->GetFullName().c_str());
                }
            }
            else if (asoptype == ASOPER_LVALUE)
            {
                if (etype != Endpoint::LHS)
                {
                    DESIGN_ERROR ("l-value of port-assignment must be "
                            "LHS endpoint or INPUT/CONTROL port",
                            edesc->owner->GetFullName().c_str());
                    DESIGN_INFO ("endpoint %s is not LHS endpoint",
                            edesc->endpt->GetName().c_str(), 
                            edesc->owner->GetFullName().c_str());
                }
            }
        }
    }

    static void PrintPortAssignValidity (Component *assigner, Pathway *path, 
            AssignOperandType asoptype, uint32_t num)
    {
        if ((asoptype == ASOPER_LVALUE && path->GetNumLHS() != num) ||
            (asoptype == ASOPER_RVALUE && path->GetNumRHS() != num))
        {
            Endpoint::Type type = (asoptype == ASOPER_LVALUE) ? 
                Endpoint::LHS : Endpoint::RHS;
            DESIGN_WARNING ("%s does not have %u %s endpoints",
                    assigner->GetFullName().c_str(), path->GetName().c_str(), 
                    num, Endpoint::GetTypeString (type));
        }
    }

    /* operators */
    PortListDescriptor operator, (PortDescriptor pdesc1, PortDescriptor pdesc2)
    { return PortListDescriptor (pdesc1, pdesc2); }

    PortListDescriptor operator, (PortListDescriptor pldesc, PortDescriptor pdesc)
    { return PortListDescriptor (pldesc, pdesc); }


    void operator>> (PortDescriptor pdesc, EndpointDescriptor edesc)
    {
        PrintPortAssignValidity (&pdesc, ASOPER_RVALUE);
        PrintPortAssignValidity (&edesc, ASOPER_LVALUE);
        pdesc.comp->Connect (pdesc.pname, edesc.endpt);
    }

    void operator>> (EndpointDescriptor edesc, PortDescriptor pdesc)
    {
        PrintPortAssignValidity (&edesc, ASOPER_RVALUE);
        PrintPortAssignValidity (&pdesc, ASOPER_LVALUE);
        pdesc.comp->Connect (pdesc.pname, edesc.endpt);
    }
        
    void operator>> (PortDescriptor pdesc1, PortDescriptor pdesc2)
    {
        PrintPortAssignValidity (&pdesc1, ASOPER_RVALUE);
        PrintPortAssignValidity (&pdesc2, ASOPER_LVALUE);

        Wire *wire = new Wire (pdesc1.owner);
        pdesc1.comp->Connect (pdesc1.pname, wire->GetEndpoint (Endpoint::LHS));
        pdesc2.comp->Connect (pdesc2.pname, wire->GetEndpoint (Endpoint::RHS));
    }
        
    void operator>> (PortListDescriptor pldesc, PortDescriptor pdesc)
    {
        PrintPortAssignValidity (&pldesc, ASOPER_RVALUE);
        PrintPortAssignValidity (&pdesc, ASOPER_LVALUE);

        RRFaninWire *path = new RRFaninWire (pldesc.owner, pldesc.pdescs.size());
        for (auto i = 0; i < pldesc.pdescs.size(); i++)
            pldesc.pdescs[i].comp->Connect (pldesc.pdescs[i].pname, 
                    path->GetEndpoint (Endpoint::LHS, i));
        pdesc.comp->Connect (pdesc.pname, path->GetEndpoint (Endpoint::RHS));
    }
        
    void operator>> (PortDescriptor pdesc, PortListDescriptor pldesc)
    {
        PrintPortAssignValidity (&pdesc, ASOPER_RVALUE);
        PrintPortAssignValidity (&pldesc, ASOPER_LVALUE);

        FanoutWire *path = new FanoutWire (pdesc.owner, pldesc.pdescs.size());
        pdesc.comp->Connect (pdesc.pname, path->GetEndpoint (Endpoint::LHS));
        for (auto i = 0; i < pldesc.pdescs.size(); i++)
            pldesc.pdescs[i].comp->Connect (pldesc.pdescs[i].pname, 
                    path->GetEndpoint (Endpoint::RHS, i));
    }


    Pathway* operator>> (PortDescriptor pdesc, Pathway *path)
    {
        PrintPortAssignValidity (&pdesc, ASOPER_RVALUE);
        PrintPortAssignValidity (pdesc.owner, path, ASOPER_LVALUE, 1);
        pdesc.comp->Connect (pdesc.pname, path->GetEndpoint (Endpoint::LHS));
        return path;
    }

    void operator>> (Pathway *path, PortDescriptor pdesc)
    {
        PrintPortAssignValidity (pdesc.owner, path, ASOPER_RVALUE, 1);
        PrintPortAssignValidity (&pdesc, ASOPER_LVALUE);
        pdesc.comp->Connect (pdesc.pname, path->GetEndpoint (Endpoint::RHS));
    }

    Pathway* operator>> (PortListDescriptor pldesc, Pathway *path)
    {
        PrintPortAssignValidity (&pldesc, ASOPER_RVALUE);
        PrintPortAssignValidity (pldesc.owner, path, ASOPER_LVALUE, pldesc.pdescs.size());
        
        for (auto i = 0; i < pldesc.pdescs.size(); i++)
            pldesc.pdescs[i].comp->Connect (pldesc.pdescs[i].pname,
                    path->GetEndpoint (Endpoint::LHS, i));

        return path;
    }

    void operator>> (Pathway *path, PortListDescriptor pldesc)
    {
        PrintPortAssignValidity (pldesc.owner, path, ASOPER_RVALUE, pldesc.pdescs.size());
        PrintPortAssignValidity (&pldesc, ASOPER_LVALUE);
        
        for (auto i = 0; i < pldesc.pdescs.size(); i++)
            pldesc.pdescs[i].comp->Connect (pldesc.pdescs[i].pname,
                    path->GetEndpoint (Endpoint::RHS, i));
    }


    void operator<< (EndpointDescriptor edesc, PortDescriptor pdesc)
    { pdesc >> edesc; }
    void operator<< (PortDescriptor pdesc, EndpointDescriptor edesc)
    { edesc >> pdesc; }
    void operator<< (PortDescriptor pdesc2, PortDescriptor pdesc1)
    { pdesc1 >> pdesc2; }
    void operator<< (PortDescriptor pdesc, PortListDescriptor pldesc)
    { pldesc >> pdesc; }
    void operator<< (PortListDescriptor pldesc, PortDescriptor pdesc)
    { pdesc >> pldesc; }

    void operator<< (Pathway *path, PortDescriptor pdesc)
    { pdesc >> path; }
    Pathway* operator<< (PortDescriptor pdesc, Pathway *path)
    { path >> pdesc; return path; }
    void operator<< (Pathway *path, PortListDescriptor pldesc)
    { pldesc >> path; }
    Pathway* operator<< (PortListDescriptor pldesc, Pathway *path)
    { path >> pldesc; return path; }
}
