#include <TSim/Module/GenericSRAM.h>
#include <TSim/Pathway/AddressMessage.h>
#include <TSim/Pathway/WordMessage.h>
#include <TSim/Utility/Prototype.h>
#include <TSim/Utility/Logging.h>
#include <TSim/Utility/StaticBranchPred.h>

#include <cinttypes>
#include <string>
#include <vector>

using namespace std;
using namespace TSim;

template <typename REGISTER>
void GenericSRAM<REGISTER>::Operation (Message **inmsgs, Message **outmsgs, 
        Instruction *instr)
{
    AddressMessage *rdaddr_msg = static_cast<AddressMessage *>(inmsgs[PORT_RDADDR]);
    AddressMessage *wraddr_msg = static_cast<AddressMessage *>(inmsgs[PORT_WRADDR]);
    WordMessage *wrdata_msg = inmsgs[PORT_WRDATA];

    if (rdaddr_msg)
    {
        uint64_t rdaddr = rdaddr_msg->addr;
        outmsgs[PORT_RDDATA] = new WordMessage (GetRegister()->GetWord (rdaddr));
    }

    if (wraddr_msg)
    {
        uint64_t wraddr = wraddr_msg->addr;
        GetRegister()->SetWord (wraddr, wrdata_msg->word);
    }
}
