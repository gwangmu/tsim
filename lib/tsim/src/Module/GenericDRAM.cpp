#include <TSim/Module/GenericDRAM.h>
#include <TSim/Pathway/DRAMRequestMessage.h>
#include <TSim/Pathway/WordMessage.h>
#include <TSim/Utility/Prototype.h>
#include <TSim/Utility/Logging.h>
#include <TSim/Utility/String.h>
#include <TSim/Utility/StaticBranchPred.h>

#include <Ramulator/DRAM.h>
#include <Ramulator/DDR4.h>
#include <Ramulator/SpeedyController.h>

#include <cinttypes>
#include <string>
#include <vector>

using namespace std;
using namespace TSim;


template <typename REGISTER>
Register::Attr GenericDRAM<REGISTER>::ConfigRamulator (Spec spec)
{
    Register::Attr attr (0, 0);
    vector<string> orgs = String::Tokenize (spec.org, "_");
    
    if (orgs[0] == "DDR4")
    {
        ramulator::Config configs ("lib/ramulator/configs/DDR4-config.cfg");
        ramulator::DDR4::Org org = ramulator::DDR4::org_map[spec.org];
        ramulator::DDR4::Speed speed = ramulator::DDR4::speed_map[spec.speed];

        ramulator::DDR4 *dram_spec = new ramulator::DDR4 (org, speed);
        dram_spec->set_channel_number (configs.get_channels ());
        dram_spec->set_rank_number (configs.get_ranks ());

        vector<ramulator::Controller<DDR4> *> ctrls;
        for (auto c = 0; c < configs.get_channels(); c++)
        {
            ramulator::DRAM<ramulator::DDR4> *channel = 
                new ramulator::DRAM<ramulator::DDR4>(dram_spec, 
                        ramulator::DDR4::Level::Channel);
            channel->id = c;
            channel->regStats("");
            ctrls.push_back (new ramulator::Controller<DDR4>(configs, channel));
        }

        raminst = new ramulator::Memory<ramulator::DDR4, ramulator::Controller>
            (configs, ctrls);

        attr.wordsize = 64;
        attr.addrsize = stoi (orgs[1].substr (0, orgs[1].length() - 2));
    }
    else
        SYSTEM_ERROR ("unsupported DRAM type");
}


template <typename REGISTER>
void GenericDRAM<REGISTER>::Operation (Message **inmsgs, Message **outmsgs, 
        Instruction *instr)
{
    DRAMRequestMessage *reqmsg = static_cast<DRAMRequestMessage *>(inmsgs[PORT_REQ]);

    if (received_word)
    {
        outmsgs[PORT_RDDATA] = new WordMessage (received_word);
        received_word = nullptr;
    }

    if (reqmsg)
    {
        if (reqmsg->type == DRAMRequestMessage::READ)
        {
            uint64_t rdaddr = reqmsg->addr;
            ramulator::Request::Type reqtype = ramulator::Request::Type::READ;

            auto complete = [rdaddr, this] (ramulator::Request &r) {
                this->ReceiveData (rdaddr);
            };

            ramulator::Request req = ramulator::Request (rdaddr, reqtype, complete);

            if (raminst->send (req))
                outmsgs[PORT_RDDATA] = Message::RESERVE();
            else
                inmsgs[PORT_REQ] = nullptr;
        }
        else if (reqmsg->type == DRAMRequestMessage::WRITE)
        {
            uint64_t wraddr = reqmsg->addr;

            ramulator::Request::Type reqtype = ramulator::Request::Type::WRITE;
            auto complete = [] (ramulator::Request &r) {};

            ramulator::Request req = ramulator::Request (wraddr, reqtype, complete);

            if (raminst->send (req))
                GetRegister()->SetWord (wraddr, reqmsg->wrdata);
            else
                inmsgs[PORT_REQ] = nullptr;
        }
    }

    raminst->tick();
}

template <typename REGISTER>
void GenericDRAM<REGISTER>::ReceiveData (uint64_t addr)
{
    if (unlikely (received_word))
        SYSTEM_ERROR ("unread DRAM word");

    received_word = GetRegister()->GetWord (addr);
}
