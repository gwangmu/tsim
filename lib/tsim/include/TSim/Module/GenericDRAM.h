#pragma once

#include <TSim/Module/Module.h>
#include <TSim/Simulation/Testbench.h>
#include <TSim/Register/Register.h>
#include <TSim/Utility/Prototype.h>

#include <Ramulator/Memory.h>

#include <cinttypes>
#include <string>
#include <vector>

using namespace std;

namespace TSim
{
    struct DRAMRequestMessage;
    struct WordMessage;
    struct RegisterWord;

    template <typename REGISTER>
    class GenericDRAM: public Module
    {
    public:
        struct Spec
        {
            string org;
            string speed;
        };

    private:
        uint32_t PORT_REQ;
        uint32_t PORT_RDDATA;

        ramulator::MemoryBase *raminst;
        RegisterWord *received_word;        

        Register::Attr ConfigRamulator (Spec spec);
        void ReceiveData (uint64_t addr); 

    public:
        GenericDRAM (Component *parent, string iname, string clkname, 
                RegisterWord *wproto, Spec spec)
            : Module ("GenericDRAM", parent, iname, 0)
        {
            SetClock (clkname);

            PORT_REQ = CreatePort ("req", Module::PORT_INPUT,
                    Prototype<DRAMRequestMessage>::Get());
            PORT_RDDATA = CreatePort ("rd_data", Module::PORT_OUTPUT,
                    Prototype<WordMessage>::Get());

            Register::Attr attr = ConfigRamulator (spec);

            SetRegister (new REGISTER (Register::DRAM, attr, wproto));
        }

        virtual void Operation (Message **inmsgs, Message **outmsgs, 
                Instruction *instr);
    };
}
