#pragma once

#include <TSim/Module/Module.h>
#include <TSim/Simulation/Testbench.h>
#include <TSim/Utility/Prototype.h>
#include <TSim/Register/Register.h>

#include <cinttypes>
#include <string>
#include <vector>

using namespace std;

namespace TSim
{
    struct AddressMessage;
    struct WordMessage;
    struct RegisterWord;

    template <typename REGISTER>
    class GenericSRAM: public Module
    {
    private:
        uint32_t PORT_RDADDR;
        uint32_t PORT_WRADDR;
        uint32_t PORT_RDDATA;
        uint32_t PORT_WRDATA;

    public:
        GenericSRAM (Component *parent, string iname, string clkname, 
                RegisterWord *wproto, uint64_t word_size, uint64_t addr_size)
            : Module ("GenericSRAM", parent, iname, 1)
        {
            SetClock (clkname);

            PORT_RDADDR = CreatePort ("rd_addr", Module::PORT_INPUT,
                    Prototype<AddressMessage>::Get());
            PORT_WRADDR = CreatePort ("wr_addr", Module::PORT_INPUT,
                    Prototype<AddressMessage>::Get());
            PORT_RDDATA = CreatePort ("rd_data", Module::PORT_OUTPUT,
                    Prototype<WordMessage>::Get());
            PORT_WRDATA = CreatePort ("wr_data", Module::PORT_INPUT,
                    Prototype<WordMessage>::Get());

            SetRegister (new REGISTER (Register::SRAM, 
                        Register::Attr (word_size, addr_size), wproto));
        }
        
        virtual void Operation (Message **inmsgs, Message **outmsgs, 
                Instruction *instr);
    };
}
