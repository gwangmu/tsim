#pragma once

#include <TSim/Register/Register.h>
#include <TSim/Utility/AccessKey.h>
#include <TSim/Utility/String.h>

#include <string>
#include <vector>
#include <cinttypes>
#include <iostream>
#include <fstream>

using namespace std;

namespace TSim
{
    struct RegisterWord;

    class AutoRegister: public Register
    {
    public:
        AutoRegister (const char *clsname, Type type, Attr attr, 
                RegisterWord *wproto);
    
        virtual const RegisterWord* GetWord (uint64_t addr)
        { Register::GetWord (addr); }
        virtual bool SetWord (uint64_t addr, RegisterWord *word)
        { Register::SetWord (addr, word); }
    };
}
