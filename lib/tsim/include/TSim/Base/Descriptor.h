#pragma once

#include <string>
#include <vector>

using namespace std;

namespace TSim
{
    class Component;
    class Pathway;
    class Endpoint;

    struct Descriptor
    {
        Descriptor (Component *owner) : owner (owner) {}
        virtual ~Descriptor () {}
        Component *owner;
    };

    struct PortDescriptor: public Descriptor
    {
        PortDescriptor (Component *owner, Component *comp, string pname)
            : Descriptor (owner), comp (comp), pname (pname) {}

        Component *comp;
        string pname;
    };

    struct PortListDescriptor: public Descriptor
    {
        PortListDescriptor (PortDescriptor pdesc1, PortDescriptor pdesc2)
            : Descriptor (pdesc1.owner)
        { pdescs.push_back (pdesc1), pdescs.push_back (pdesc2); }

        PortListDescriptor (PortListDescriptor pldesc, PortDescriptor pdesc)
            : Descriptor (pldesc.owner)
        { pdescs = pldesc.pdescs, pdescs.push_back (pdesc); }

        vector<PortDescriptor> pdescs;
    };

    struct EndpointDescriptor: public Descriptor
    {
        EndpointDescriptor (Component *owner, Endpoint *endpt)
            : Descriptor (owner), endpt (endpt) {}

        Endpoint *endpt;
    };


    /* operators */
    PortListDescriptor operator, (PortDescriptor pdesc1, PortDescriptor pdesc2);
    PortListDescriptor operator, (PortListDescriptor pldesc, PortDescriptor pdesc);

    void operator>> (PortDescriptor pdesc, EndpointDescriptor edesc);
    void operator>> (EndpointDescriptor edesc, PortDescriptor pdesc);
    void operator>> (PortDescriptor pdesc1, PortDescriptor pdesc2);
    void operator>> (PortListDescriptor pldesc, PortDescriptor pdesc);
    void operator>> (PortDescriptor pdesc, PortListDescriptor pldesc);

    Pathway* operator>> (PortDescriptor pdesc, Pathway *path);
    void operator>> (Pathway *path, PortDescriptor pdesc);
    Pathway* operator>> (PortListDescriptor pldesc, Pathway *path);
    void operator>> (Pathway *path, PortListDescriptor pldesc);

    void operator<< (EndpointDescriptor edesc, PortDescriptor pdesc);
    void operator<< (PortDescriptor pdesc, EndpointDescriptor edesc);
    void operator<< (PortDescriptor pdesc2, PortDescriptor pdesc1);
    void operator<< (PortDescriptor pdesc, PortListDescriptor pldesc);
    void operator<< (PortListDescriptor pldesc, PortDescriptor pdesc);

    void operator<< (Pathway *path, PortDescriptor pdesc);
    Pathway* operator<< (PortDescriptor pdesc, Pathway *path);
    void operator<< (Pathway *path, PortListDescriptor pldesc);
    Pathway* operator<< (PortListDescriptor pldesc, Pathway *path);
}
