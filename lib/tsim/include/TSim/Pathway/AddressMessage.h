#pragma once

#include <TSim/Pathway/Message.h>
#include <cinttypes>

using namespace std;

namespace TSim
{
    struct AddressMessage : public Message
    {
    public:
        // NOTE: fixed to broadcast
        AddressMessage () 
            : Message ("AddressMessage", PLAIN, -1), addr (-1) {}
        AddressMessage (uint64_t addr)
            : Message ("AddressMessage", PLAIN, -1), addr (addr) {}
    
        uint64_t addr;
    };
}
