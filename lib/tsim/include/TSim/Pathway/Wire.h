#pragma once

#include <TSim/Pathway/Pathway.h>

#include <cinttypes>
#include <string>
#include <vector>

using namespace std;

namespace TSim
{
    class Component;
    class Message;

    class Wire: public Pathway
    {
    public:
        Wire (Component *parent, ConnectionAttr conattr = ConnectionAttr(0, -1),
                Message *msgproto = nullptr);
        virtual uint32_t NextTargetLHSEndpointID ();
    };
}
