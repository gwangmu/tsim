#pragma once

#include <TSim/Pathway/Message.h>
#include <cinttypes>

using namespace std;

namespace TSim
{
    struct RegisterWord;

    struct WordMessage : public Message
    {
    public:
        // NOTE: fixed to broadcast
        WordMessage () 
            : Message ("WordMessage", PLAIN, -1), word (nullptr) {}
        WordMessage (RegisterWord *word)
            : Message ("WordMessage", PLAIN, -1), word (word) {}
    
        RegisterWord *word;
    };
}
