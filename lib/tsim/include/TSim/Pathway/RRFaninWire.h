#pragma once

#include <TSim/Pathway/Pathway.h>

#include <cinttypes>
#include <string>
#include <vector>

using namespace std;

namespace TSim
{
    class Component;
    class Message;

    class RRFaninWire: public Pathway
    {
    public:
        RRFaninWire (Component *parent, ConnectionAttr conattr, Message *msgproto, 
                uint32_t n_lhs);
        RRFaninWire (Component *parent, ConnectionAttr conattr, uint32_t n_lhs)
            : RRFaninWire (parent, conattr, nullptr, n_lhs) {}
        RRFaninWire (Component *parent, uint32_t n_lhs)
            : RRFaninWire (parent, ConnectionAttr(0, -1), nullptr, n_lhs) {}
        virtual uint32_t NextTargetLHSEndpointID ();
    
    private:
        uint32_t cur_lhsid;
    };
}
