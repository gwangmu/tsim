#pragma once

#include <TSim/Pathway/Message.h>
#include <cinttypes>

using namespace std;

namespace TSim
{
    struct DRAMRequestMessage : public Message
    {
    public:
        // NOTE: fixed to broadcast
        DRAMRequestMessage () 
            : Message ("DRAMRequestMessage", PLAIN, -1), addr (-1) {}
        DRAMRequestMessage (uint64_t addr)
            : Message ("DRAMRequestMessage", PLAIN, -1), addr (addr) {}
    
        enum { READ, WRITE } type;
        uint64_t addr;
        RegisterWord *wrdata;
    };
}
