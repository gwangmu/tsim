#pragma once

#include <TSim/Device/Gate.h>
#include <TSim/Utility/Prototype.h>
#include <TSim/Pathway/IntegerMessage.h>
#include <TSim/Utility/Logging.h>

#include <cinttypes>
#include <string>
#include <vector>

using namespace std;

namespace TSim
{
    class Message;
    class Component;
    class Simulator;

    class AndGate: public Gate
    {
    public:
        AndGate (Component *parent, string iname, uint32_t ninput)
            : Gate ("AndGate", parent, iname, Prototype<IntegerMessage>::Get(), ninput), 
            cached (false) {}

        // NOTE: backward-compatibility
        AndGate (string iname, Component *parent, uint32_t ninput)
            : AndGate (parent, iname, ninput) {}
    
        virtual Message* Logic (Message const * const *inmsgs);
    
    private:
        IntegerMessage cached_output;
        bool cached;
    };
}
