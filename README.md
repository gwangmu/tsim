# TSim - Cycle-accurate Full System Simulator Framework

TSim is a cycle-accurate full system simulator framework that allows you a fast design space exploration.

## Getting Started

### Prerequisites

* clang/clang++ (over version 3.8)

### Installing

Building TSim is as easy as just typing 'make' at the base directory.

```
make -j12  # for a parallel compilation
```

This will generate a 'libtsim.a' static library that you can attach to your design source code. Link this library when you compile your own design source code. For example,

```
clang++ --std=c++11 <your_design_sources> libtsim.a  # '--std=c++11' is required
```

## How to Use (Summary)

### Concept

TSim employes a Verilog-like semantics with some restrictions for the sake of a simple prototyping. As the smallest building blocks, *modules* have their own input/output ports and clock-synchronous operation descriptions. During the operation, a module accept *messages* sent from others, perform some operations (or just *pretend* to perform by delaying cycles), and transmit new messages through the output ports. 

*Pathways* are the entities that connect modules, passing messages between them. A pathway has two types of *endpoints*: LHS and RHS. LHS endpoints only accept messages from modules, and are only compatible to the output ports. Conversely, RHS endpoints only transmit messages to modules, and are only compatible to the input ports. Each endpoint has an internal FIFO queue to reserve messages being sent or received, and the sizes of them are configurable.

*Components* are the containers of modules, pathways, and yet other components. A component can export the internal modules' ports to the outside.

*Testbenches* specifies the top-level component representing the whole design, and a terminal condition that determines the simulator to finish. The TSim framework refers to the testbench to load a design and determine the simulation progress.

### Composing Design

(To be filled)


## Authors

* **Gwangmu Lee** -*initial work*- [HPCS Lab. @ SNU](https://hpcs.snu.ac.kr)
* **Sunghwa Lee** -*testing*- [HPCS Lab. @ SNU](https://hpcs.snu.ac.kr)


## License

This project is licenced under the MIT License.
