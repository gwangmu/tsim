BIN=simulator

### Auxiliaries ###
# characters
NULL:=
COMMA:=,
SPACE:=$(NULL) #

# bash commands
MKDIR=mkdir -p
RM=rm -rf

# extensions
CPPEXT=cpp
HDREXT=h
OBJEXT=o
###################

### Toolchain ###
# compiler commands
CXX=clang++
LD=clang++
AR=ar
MAKE=make --no-print-directory

# compiler flags
CXXFLAGS=--std=c++11 -O3 -ferror-limit=3 -DRAMULATOR $(if $(NDEBUG),-DNDEBUG) $(if $(NINFO),-DNINFO) -g 
LDFLAGS=-O3
ARFLAGS=-rc
#################

### Paths ###
# root directories
SRCDIR=src
OBJDIR=obj
HDRDIR=include
LIBDIR=lib

ifdef EXAMPLE
SRCDIR=example/$(EXAMPLE)/src
HDRDIR=example/$(EXAMPLE)/include
endif

# library paths
TSIM=TSim
TSIM_DIR=$(LIBDIR)/tsim
TSIM_HDRDIR=$(TSIM_DIR)/include/TSim
TSIM_LIB=$(TSIM_DIR)/libtsim.a
TSIM_MAKEFILE_DIR=$(TSIM_DIR)


# TODO: add variables for new library
#[lib]=[lib_name]
#[lib]_DIR=$(LIBDIR)/[lib_dirname]
#[lib]_HDRDIR=$([lib]_DIR)/[lib_hdrdir_name]
#[lib]_LIB=$([lib]_DIR)/[lib_bindir_name]

# libraies in use
USING_LIBS=TSIM  		# TODO: add [lib] in this list.
USING_LIB_PATHS=$(foreach LIBVAR,$(USING_LIBS),$($(LIBVAR)_LIB))
#############

### Helper functions ###
init_print=rm -rf /tmp/.tsim_make
print=until mkdir /tmp/.tsim_make 2> /dev/null; do true; done; \
	printf "info: %-15s " $(1); \
	STRING="$(if $(2),[,) $(2)$(if $(3), --> $(3),) $(if $(2),],)"; \
	N=0; \
	for WORD in $$STRING; do \
		WORDLEN=`expr length "$$WORD "`; \
		if [ $$N -ne 0 ] && (( N + WORDLEN > 72 )); then \
			echo; \
			printf "%24s" ' '; \
			N=0; \
		fi; \
		printf "%s " "$$WORD"; \
		(( N = N + WORDLEN )); \
	done; echo; \
	rmdir /tmp/.tsim_make;
to_comma_list=$(subst $(SPACE),$(COMMA) ,$(1))
########################


### Makefile lists ###
# source and object files
SRCSUBDIRS:=$(shell find $(SRCDIR) -type d)
OBJSUBDIRS:=$(subst $(SRCDIR),$(OBJDIR),$(SRCSUBDIRS))
CODEFILES:=$(addsuffix /*,$(SRCSUBDIRS))
CODEFILES:=$(wildcard $(CODEFILES))

SRCFILES:=$(filter %.$(CPPEXT),$(CODEFILES))
OBJFILES:=$(subst $(SRCDIR),$(OBJDIR),$(SRCFILES:%.$(CPPEXT)=%.$(OBJEXT)))
RMFILES:=$(OBJDIR) $(BIN)
######################


### rules
## simulator
all: $(OBJSUBDIRS)
	@ $(call init_print)
	@ $(MAKE) $(USING_LIBS)
	@ $(call print,"making..",$(BIN))
	@ $(MAKE) $(BIN)
	@ $(call print,"done.")


$(USING_LIBS):
	@ $(call print,$(if $(IS_CLEANING),"cleaning..","making.."),$($@))
	@ $(if $($@_MAKEFILE_DIR), \
		$(MAKE) -C $($@_MAKEFILE_DIR) $(if $(IS_CLEANING),clean,\
				$(if $(IS_HYPERCLEANING),superclean,)) \
			$(shell ln -sf $(abspath $($@_HDRDIR) $(HDRDIR)/$($@))), \
		$(if $(IS_CLEANING),$@_clean,$@))

# TODO: create '<LIB_VAR_NAME>_clean' rule here
#  if library has no makefile

$(OBJSUBDIRS):
	@ $(call print,"creating..",$(call to_comma_list,$@))
	@ $(MKDIR) $@

$(BIN): $(OBJFILES) $(USING_LIB_PATHS)
	@ $(call print,"linking..",$(call to_comma_list,$^),$@)
	@ $(LD) $^ -o $@


$(OBJDIR)/%.$(OBJEXT): $(SRCDIR)/%.$(CPPEXT)
	@ $(call print,"compiling..",$<,$@)
	@ $(CXX) -I$(HDRDIR) $(CXXFLAGS) -c $^ -o $@


## clean
clean:
	@ $(call init_print)
	@ $(call print,"removing..",$(call to_comma_list,$(RMFILES)))
	@ $(RM) $(RMFILES)
	@ $(call print,"done.")

superclean:
	@ $(MAKE) $(USING_LIBS) IS_CLEANING=1
	@ $(MAKE) clean

hyperclean:
	@ $(MAKE) $(USING_LIBS) IS_HYPERCLEANING=1
	@ $(MAKE) clean
