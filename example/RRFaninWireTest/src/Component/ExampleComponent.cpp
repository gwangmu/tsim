#include <TSim/Pathway/Wire.h>
#include <TSim/Pathway/RRFaninWire.h>
#include <TSim/Utility/Prototype.h>

#include <Component/ExampleComponent.h>
#include <Component/DataSourceModule.h>
#include <Component/DataSinkModule.h>
#include <Message/ExampleMessage.h>

#include <cinttypes>
#include <string>
#include <vector>

using namespace std;


ExampleComponent::ExampleComponent (Component *parent, string iname)
    : Component ("ExampleComponent", parent, iname)
{
    // NOTE: children automatically inherit parent's clock
    //  but they can override it by redefining their own.
    SetClock ("main");

    // add child modules/components
    Module *datasource = Create<DataSourceModule>("datasource");
    Module *datasource2 = Create<DataSourceModule>("datasource2");
    Module *datasink = Create<DataSinkModule>("datasink");

    // connect modules
    (Port(datasource, "dataout"), Port(datasource2, "dataout"))
        >> Port(datasink, "datain");
}
