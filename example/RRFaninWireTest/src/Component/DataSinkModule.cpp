#include <TSim/Utility/Prototype.h>
#include <TSim/Utility/Logging.h>

#include <Script/ExampleFileScript.h>
#include <Component/DataSinkModule.h>
#include <Message/ExampleMessage.h>

#include <cinttypes>
#include <string>
#include <vector>

using namespace std;


DataSinkModule::DataSinkModule (Component *parent, string iname)
    : Module ("DataSinkModule", parent, iname, 1)
{
    // create ports
    PORT_DATAIN = CreatePort<ExampleMessage>("datain", PORT_INPUT);

    recvdata = 0;
}

// NOTE: called only if not stalled
void DataSinkModule::Operation (Message **inmsgs, Message **outmsgs, Instruction *instr)
{
    ExampleMessage *inmsg = static_cast<ExampleMessage *>(inmsgs[PORT_DATAIN]);

    if (inmsg) 
    {
        recvdata = inmsg->value;
        DEBUG_PRINT ("val = %u,", recvdata);
    }
}
